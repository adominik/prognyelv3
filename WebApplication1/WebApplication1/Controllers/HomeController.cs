﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using WebApplication1.Resources;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Foo = "FOOS";
            return View();
        }

        public ActionResult ReadFile()
        {
            string param = Request.QueryString["type"];
            if (param == null)
            {
                param = "30";
            }
            List<Medicine> mList = new List<Medicine>();
            System.IO.StreamReader file = new System.IO.StreamReader(Server.MapPath(@"~/App_Data/input.json"));
            String content = file.ReadToEnd();
            try
            {
                //File beolvasás jsonból LIST-be
                MedicinList medicinWrapper = JsonConvert.DeserializeObject<MedicinList>(content);
                try
                {
                     var result = from elem in medicinWrapper.Medicines
                             where elem.IllnessCode == param
                             orderby elem.Price
                             select elem;

                int qResultCount = result.Count();
                if (qResultCount >= 0)
                {
                    ViewData["queryData"] = result.First().ToString();
                }
                }catch(Exception ex2){
                    ViewData["queryData"] = "A keresés nem járt sikerrel!";
                }
               
               
                //return Content(mList.ToString());
                //return Content(x);
                ViewData["medicineWrapper"] = medicinWrapper.Medicines;
                ViewData["queryType"] = param;
                //ViewData["queryData"] = qResultCount.ToString();
                ViewData["medicineSize"] = medicinWrapper.Medicines.Count().ToString();
                return View();
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
           
                      
           //medicinWrapper.Medicines.OrderBy(s=>s.Price).FirstOrDefault()

            

            

            

            /*int counter = 0;
            string line;
            List<string> sor = new List<string>();


            System.IO.StreamReader file = new System.IO.StreamReader(Server.MapPath(@"~/App_Data/input.txt"));
            while ((line = file.ReadLine()) != null)
            {
              sor.Add(line);
               
                counter++;
            }
           

            file.Close();
            
           

            return Content("LOFASZ");*/

            
        }

        public ActionResult ReadMedicines()
        {
            List<Medicine> mList = new List<Medicine>();
            System.IO.StreamReader file = new System.IO.StreamReader(Server.MapPath(@"~/App_Data/input.txt"));

            while(!file.EndOfStream){
                mList.Add(Medicine.ReadFromStream(file));
            }

            return Content(mList.ToString());
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}