﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApplication1.Resources
{
    public class Medicine
    {
        private List<String> substances = new List<string>();
        private List<String> sideEffects = new List<string>();

        public string Name
        {
            get;
            set;
        }

        public int Price
        {
            get;
            set;
        }

        public string IllnessCode
        {
            get;
            set;
        }

        public List<string> Substances
        {
            get;
            set;
        }

        public List<string> SideEffects
        {
            get;
            set;
        }

        public static Medicine ReadFromStream(StreamReader reader)
        {
            Medicine medicine = new Medicine();
            medicine.Name = reader.ReadLine();
            medicine.Price = int.Parse(reader.ReadLine());
            medicine.IllnessCode = reader.ReadLine();
            String[] tmp = reader.ReadLine().Split(' ');
            foreach (String s in tmp)
            {
                medicine.substances.Add(s);
            }
            String[] tmp2 = reader.ReadLine().Split(' ');
            foreach (String s in tmp2)
            {
                medicine.sideEffects.Add(s);
            }
            return medicine;
        }

        public override String ToString()
        {

            string returnString = "<p>Név: " + Name + "</p>" + "<p>Ár: " + Price + "</p><p>Code" + IllnessCode + "</p><p>Substances:</p><ul>";
            for (int i = 0; i < Substances.Count(); i++)
            {
                returnString += "<li>"+Substances[i]+"</li>";
            }
            returnString += "</ul><p>SideEffects:</p><ul>";
            for (int i = 0; i < SideEffects.Count(); i++)
            {
                returnString += "<li>" + SideEffects[i] + "</li>";
            }
            returnString += "</ul>";
            
                return returnString;
        }
    }
}

/*
Gy1
1200
30
h1 h2 h3 h4
m1 m2 m3 m4
Gy2
2000
30
h3 h4
m3 m4
Gy3
500
30
h1 h4
m1 m3 m4
Gy4
1000
30
h1 h3 h4
m1 m2 m4
*/